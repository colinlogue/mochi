//! Interpreter for the Mochi programming language.

/// Basic definition of terms, types and values.
pub mod ast;

/// Evaluation of Mochi expressions.
pub mod eval;

/// Parse a raw string into a Mochi program.
pub mod parse;

/// Translate a Mochi program into an `iced::Application`.
pub mod runtime;

/// Type checking and inference.
pub mod types;

/// Definitions for the base library for primitive operations.
pub mod base;
