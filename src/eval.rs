use crate::ast::*;
use std::collections::HashMap;

/// An evaluation context that maps variable names to their values
pub type Context = HashMap<String, Value>;

/// Helper for evaluating primitive operations
fn eval_primop(op: PrimOp, args: &[Value]) -> Value {
  match (op, args) {
    (PrimOp::Add, [Value::Int(v1), Value::Int(v2)]) => {
      Value::Int(v1 + v2)
    }
    (PrimOp::Sub, [Value::Int(v1), Value::Int(v2)]) => {
      Value::Int(v1 - v2)
    }
    (PrimOp::Mul, [Value::Int(v1), Value::Int(v2)]) => {
      Value::Int(v1 * v2)
    }
    (PrimOp::Add, [Value::Float(v1), Value::Float(v2)]) => {
      Value::Float(v1 + v2)
    }
    (PrimOp::Sub, [Value::Float(v1), Value::Float(v2)]) => {
      Value::Float(v1 - v2)
    }
    (PrimOp::Mul, [Value::Float(v1), Value::Float(v2)]) => {
      Value::Float(v1 * v2)
    }
    (PrimOp::StrAppend, [Value::String(s1), Value::String(s2)]) => {
      let mut s = String::from(s1);
      s.push_str(s2);
      Value::String(s)
    }
    (PrimOp::IntToStr, [Value::Int(v)]) => Value::String(v.to_string()),
    _ => Value::Prim(op, Vec::from(args)),
  }
}

// sub s for x in e
fn subst(x: &str, s: &Term, e: &Term) -> Term {
  use term::*;
  match e {
    Term::App(e1, e2) => {
      let e1 = subst(x, s, e1);
      let e2 = subst(x, s, e2);
      app(e1, e2)
    }
    Term::Ascr(e, t) => {
      let e = subst(x, s, e);
      Term::Ascr(Box::new(e), t.clone())
    }
    Term::If(guard, e1, e2) => {
      let guard = subst(x, s, guard);
      let e1 = subst(x, s, e1);
      let e2 = subst(x, s, e2);
      Term::If(Box::new(guard), Box::new(e1), Box::new(e2))
    }
    Term::Lambda(y, body) => {
      if *x == *y {
        lambda(x, *body.clone())
      } else {
        lambda(y, subst(x, s, body))
      }
    }
    Term::Literal(l) => Term::Literal(l.clone()),
    Term::Proj(e, label) => {
      let e = subst(x, s, e);
      proj(e, label)
    }
    Term::Record(fields) => {
      let fields = fields
        .iter()
        .map(|(y, e)| (y.clone(), subst(x, s, e)))
        .collect();
      Term::Record(fields)
    }
    Term::Var(y) => {
      if x == y {
        s.clone()
      } else {
        var(y)
      }
    }
    Term::Local(defs, body) => {
      // if x is shadowed in local defs, do nothing
      if defs.contains_key(x) {
        e.clone()
      }
      // otherwise subst recursively
      else {
        Term::Local(defs.clone(), Box::new(subst(x, s, body)))
      }
    }
  }
}

/// Evaluate an expression in some context.
///
/// # Arguments
///
/// * `cxt` - The substitution context, a map from variable names to
///   their values
/// * `e` - A reference to the expression (a `Term`) to evaluate
///
/// # Examples
///
/// ```
/// use mochi::ast::term::*;
/// use mochi::ast::Value::Int;
/// use mochi::eval::eval;
/// use std::collections::HashMap;
///
/// let empty_cxt = HashMap::new();
///
/// // (fun x => x) 42
/// let e = app(lambda("x", var("x")), int(42));
///
/// assert_eq!(eval(&empty_cxt, &e), Int(42));
/// ```
///
/// # Panics
///
/// Panics when evaluation gets stuck. This is guaranteed not to happen
/// if the following conditions are met:
///
/// * `e` is well-typed in relation to a typing context that assigns all
///   of its variables to types.
/// * Each value in `cxt` is also well-typed under the same context.
///
/// This means that if the evaluation and typing contexts are derived
/// from the same set of definitions, and all of the definitions pass
/// the typechecker under that context, then evaluation will not panic.
///
pub fn eval(cxt: &Context, e: &Term) -> Value {
  match e {
    Term::Var(x) => match cxt.get(x) {
      Some(v) => v.clone(),
      None => panic!("unknown var: {}", x),
    },
    Term::App(e1, e2) => match eval(cxt, e1) {
      Value::Lambda(x, body) => {
        let e = subst(x.as_str(), e2, &body);
        eval(cxt, &e)
      }
      Value::Prim(p, args) => {
        let mut args = args;
        let v = eval(cxt, e2);
        args.push(v);
        eval_primop(p, &args)
      }
      Value::Element(el, args) => {
        let mut args = args;
        let v = eval(cxt, e2);
        args.push(v);
        Value::Element(el, args)
      }
      _ => panic!("attempted to apply non-function term: {:?}", e1),
    },
    Term::Lambda(x, body) => Value::Lambda(x.clone(), (**body).clone()),
    Term::Ascr(e1, _) => eval(cxt, e1),
    Term::Literal(l) => l.as_value(),
    Term::Record(fields) => Value::Record(
      fields
        .iter()
        .map(|(x, e1)| (x.clone(), eval(cxt, e1)))
        .collect(),
    ),
    Term::Proj(e1, label) => match eval(cxt, e1) {
      Value::Record(fields) => fields
        .get(label)
        .unwrap_or_else(|| panic!("unknown field label: {}", label))
        .clone(),
      _ => panic!("not a record: {:?}", e1),
    },
    Term::If(guard, e1, e2) => match eval(cxt, guard) {
      Value::Bool(b) => {
        let e = if b { e1 } else { e2 };
        eval(cxt, e)
      }
      _ => panic!("if guard is not bool"),
    },
    Term::Local(defs, body) => {
      let mut local_cxt = cxt.clone();
      // todo: should order matter in local definitions?
      for (x, e) in defs {
        let v = eval(&local_cxt, e);
        local_cxt.insert(x.clone(), v);
      }
      eval(&local_cxt, body)
    }
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  use crate::base;
  use crate::parse::parse_term;
  use term::*;

  #[test]
  fn subst_test1() {
    let e = lambda(
      "model",
      Term::Record(
        [(
          String::from("counter"),
          app(
            app(var("add"), proj(var("model"), "counter")),
            proj(var("msg"), "change"),
          ),
        )]
        .iter()
        .cloned()
        .collect(),
      ),
    );
    let msg = Term::Record(
      [(String::from("change"), int(1))].iter().cloned().collect(),
    );
    let actual = subst("msg", &msg, &e);
    let expected = lambda(
      "model",
      Term::Record(
        [(
          String::from("counter"),
          app(
            app(var("add"), proj(var("model"), "counter")),
            proj(msg, "change"),
          ),
        )]
        .iter()
        .cloned()
        .collect(),
      ),
    );
    assert_eq!(actual, expected);
  }

  #[test]
  fn subst_test2() {
    assert_eq!(subst("x", &int(42), &var("x")), int(42));
  }

  #[test]
  fn test1() {
    let e = lambda(
      "msg",
      lambda(
        "model",
        Term::Record(
          [(
            String::from("counter"),
            app(
              app(var("add"), proj(var("model"), "counter")),
              proj(var("msg"), "change"),
            ),
          )]
          .iter()
          .cloned()
          .collect(),
        ),
      ),
    );
    let msg = Term::Record(
      [(String::from("change"), int(1))].iter().cloned().collect(),
    );
    let model = Term::Record(
      [(String::from("counter"), int(41))]
        .iter()
        .cloned()
        .collect(),
    );
    let actual = eval(&base::VAL_CONTEXT, &app(app(e, msg), model));
    let expected = Value::Record(
      [(String::from("counter"), Value::Int(42))]
        .iter()
        .cloned()
        .collect(),
    );
    assert_eq!(actual, expected);
  }

  #[test]
  fn eval_local1() {
    let e = parse_term("let { x : Int; x = 1 } in x").unwrap();
    assert_eq!(eval(&base::VAL_CONTEXT, &e), Value::Int(1));
  }

  #[test]
  fn eval_local2() {
    let e = parse_term(
      "let { id : Int -> Int; id x = x ; x : Int; x = 42 } in id x",
    )
    .unwrap();
    assert_eq!(eval(&base::VAL_CONTEXT, &e), Value::Int(42));
  }
}
