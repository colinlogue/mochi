use crate::ast::*;
use iced::{Button, Element};
use lazy_static::lazy_static;
use std::collections::HashMap;
use types::*;

#[derive(Clone, Copy, Debug)]
pub enum BaseElem {
  Column,
  Row,
  Text,
  Button,
}

mod storage {

  use iced::button;

  static mut STORAGE: Vec<button::State> = vec![];

  pub fn new_state() -> &'static mut button::State {
    unsafe {
      let idx = STORAGE.len();
      let state = button::State::new();
      STORAGE.push(state);
      &mut STORAGE[idx]
    }
  }
}

lazy_static! {
  static ref BUTTON_FIELDS: HashMap<std::string::String, Type> = [
    ("label".into(), types::prim(PrimType::Element)),
    ("onPress".into(), Type::Named("Message".into())),
  ]
  .iter()
  .cloned()
  .collect();
}

lazy_static! {
  /// The base context exposing primitives to user programs.
  pub static ref CONTEXT: HashMap<&'static str, (Value, Type)> = [
    (
      "column",
      (
        Value::Element(Box::new(BaseElem::Column), vec![]),
        variadic(prim(PrimType::Element), prim(PrimType::Element))
      )
    ),
    (
      "row",
      (
        Value::Element(Box::new(BaseElem::Row), vec![]),
        variadic(prim(PrimType::Element), prim(PrimType::Element))
      )
    ),
    (
      "text",
      (
        Value::Element(Box::new(BaseElem::Text), vec![]),
        arrow(prim(PrimType::String), prim(PrimType::Element))
      )
    ),
    (
      "button",
      (
        Value::Element(Box::new(BaseElem::Button), vec![]),
        arrow(Type::Record(BUTTON_FIELDS.clone()),prim(PrimType::Element))
      )
    ),
    ("true", (Value::Bool(true), prim(PrimType::Bool))),
    ("false", (Value::Bool(false), prim(PrimType::Bool))),
    ("add", (Value::Prim(PrimOp::Add, vec![]),
      arrow(prim(PrimType::Int), arrow(prim(PrimType::Int),prim(PrimType::Int))))),
    ("sub", (Value::Prim(PrimOp::Sub, vec![]),
      arrow(prim(PrimType::Int), arrow(prim(PrimType::Int),prim(PrimType::Int))))),
    ("mul", (Value::Prim(PrimOp::Mul, vec![]),
      arrow(prim(PrimType::Int), arrow(prim(PrimType::Int),prim(PrimType::Int))))),
    ("app", (Value::Prim(PrimOp::StrAppend, vec![]),
      arrow(prim(PrimType::String), arrow(prim(PrimType::String), prim(PrimType::String))))),
    ("intToStr", (Value::Prim(PrimOp::IntToStr, vec![]),
      arrow(prim(PrimType::Int), prim(PrimType::String)))),
    ]
  .iter()
  .cloned()
  .collect();
}

lazy_static! {
  pub static ref TYPE_CONTEXT: crate::types::Context = CONTEXT
    .iter()
    .map(|(x, (_, t))| (std::string::String::from(*x), t.clone()))
    .collect();
}

lazy_static! {
  pub static ref VAL_CONTEXT: crate::eval::Context = CONTEXT
    .iter()
    .map(|(x, (v, _))| (std::string::String::from(*x), v.clone()))
    .collect();
}

impl Elem for BaseElem {
  fn render(&self, args: &[Value]) -> Element<'static, Value> {
    match (self, args) {
      (BaseElem::Button, [Value::Record(fields)]) => {
        let label = fields.get("label").unwrap().render();
        let on_press = fields.get("onPress").unwrap();
        let state = storage::new_state();
        Button::new(state, label).on_press(on_press.clone()).into()
      }
      (BaseElem::Row, args) => iced::Row::with_children(
        args.iter().map(|e| e.render()).collect(),
      )
      .into(),
      (BaseElem::Column, args) => iced::Column::with_children(
        args.iter().map(|e| e.render()).collect(),
      )
      .into(),
      (BaseElem::Text, [Value::String(s)]) => iced::Text::new(s).into(),
      _ => panic!("invalid args to base element"),
    }
  }

  fn clone_as_box(&self) -> Box<dyn Elem> {
    Box::new(*self)
  }

  fn as_term(&self) -> Term {
    match self {
      BaseElem::Button => term::var("button"),
      BaseElem::Column => term::var("column"),
      BaseElem::Row => term::var("row"),
      BaseElem::Text => term::var("text"),
    }
  }
}
