use crate::ast::*;
use crate::eval::*;
use iced::*;

pub struct MochiApp {
  model: Value,
  view_val: Value,
  update_val: Value,
  context: Context,
}

impl MochiApp {
  pub fn new(
    init: Value,
    view: Value,
    update: Value,
    cxt: Context,
  ) -> Self {
    MochiApp {
      model: init,
      view_val: view,
      update_val: update,
      context: cxt,
    }
  }
}

impl Application for MochiApp {
  type Executor = executor::Default;
  type Message = Value;
  type Flags = (Value, Value, Value, Context);

  fn new(
    (init, view, update, cxt): Self::Flags,
  ) -> (Self, Command<Value>) {
    (MochiApp::new(init, view, update, cxt), Command::none())
  }

  fn title(&self) -> String {
    String::from("Mochi App")
  }

  fn update(
    &mut self,
    msg: Value,
    _clipboard: &mut Clipboard,
  ) -> Command<Value> {
    let e = Term::App(
      Box::new(Term::App(
        Box::new(self.update_val.as_term()),
        Box::new(msg.as_term()),
      )),
      Box::new(self.model.as_term()),
    );
    self.model = eval(&self.context, &e);
    Command::none()
  }

  fn view(&mut self) -> Element<'_, Value> {
    let e = Term::App(
      Box::new(self.view_val.as_term()),
      Box::new(self.model.as_term()),
    );
    eval(&self.context, &e).render()
  }
}
