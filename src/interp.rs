use iced::settings::Settings;
use iced::Application;
use mochi::ast::*;
use mochi::base;
use mochi::eval;
use mochi::parse::*;
use mochi::runtime::*;
use mochi::types;
use std::fs;

/// Check that the program provides the necessary definitions.
///
/// A program must provide a `Model` and `Message` type alias and
/// view`, `update` and `init` definitions.
///
/// Note that this does not currently check that the functions have the
/// correct types.
///
/// # Panics
///
/// When any of the necessary definitions are not found.
///
/// # Notes
///
/// This function will be removed in the future and replaced with a
/// procedure to check that there is a `main` with a valid program type,
/// although what specifically that means is still undetermined.
///
fn validate_prog(prog: &[Declaration]) {
  // must have type alias for model and message
  let mut has_model = false;
  let mut has_msg = false;
  let mut has_view = false;
  let mut has_update = false;
  let mut has_init = false;
  for d in prog {
    match d {
      Declaration::TypeAlias(s, _) => {
        if s == "Model" {
          has_model = true;
        } else if s == "Message" {
          has_msg = true;
        }
      }
      // todo: validate correct types as well
      Declaration::Definition(x, _, _) => {
        if x == "view" {
          has_view = true;
        }
        if x == "update" {
          has_update = true;
        }
        if x == "init" {
          has_init = true;
        }
      }
    }
  }
  if !has_model {
    panic!("no Model alias declared");
  }
  if !has_msg {
    panic!("no Message alias declared");
  }
  if !has_init {
    panic!("init is not defined");
  }
  if !has_view {
    panic!("view is not defined");
  }
  if !has_update {
    panic!("update is not defined");
  }
}

/// Load a Mochi file, check that it represents a valid program, then
/// interpret and run it as a GUI app.
pub fn main() -> iced::Result {
  // load file
  let filepath = std::env::args().nth(1).unwrap();
  let raw = fs::read_to_string(filepath).expect("failed to read file");
  let prog = parse_program(raw.as_str()).expect("invalid program");

  // check that required definitions are available
  validate_prog(&prog);

  // build contexts and typecheck
  let mut type_cxt: types::Context = base::CONTEXT
    .iter()
    .map(|(x, (_, t))| (String::from(*x), t.clone()))
    .collect();
  let mut type_aliases = types::BASE_ALIASES.clone();
  let mut eval_cxt: eval::Context = base::CONTEXT
    .iter()
    .map(|(x, (v, _))| (String::from(*x), v.clone()))
    .collect();

  let mut defs = Vec::new();

  // first pass just adds types and aliases
  for d in prog {
    match d {
      Declaration::Definition(x, t, e) => {
        type_cxt.insert(x.clone(), t.clone());
        defs.push((x.clone(), e.clone(), t.clone()));
      }
      Declaration::TypeAlias(x, t) => {
        let t = types::drop_aliases(&type_aliases, &t);
        type_aliases.insert(x.clone(), t.clone());
      }
    }
  }

  type_cxt = type_cxt
    .iter()
    .map(|(x, t)| (x.clone(), types::drop_aliases(&type_aliases, t)))
    .collect();
  eval_cxt = eval_cxt
    .iter()
    .map(|(x, e)| {
      (x.clone(), types::drop_value_aliases(&type_aliases, e))
    })
    .collect();

  // second pass to typecheck definitions
  for (x, e, t) in defs {
    let t = types::drop_aliases(&type_aliases, &t);
    let e = types::drop_term_aliases(&type_aliases, &e);
    match types::check(&type_cxt, &e, &t) {
      Ok(_) => (),
      Err(e) => panic!("typechecking failed for {}: {:?}", x, e),
    }
    eval_cxt.insert(x.clone(), eval::eval(&eval_cxt, &e));
  }

  let init_val = eval_cxt.get("init").unwrap().clone();
  let view_func = eval_cxt.get("view").unwrap().clone();
  let update_func = eval_cxt.get("update").unwrap().clone();

  let settings = Settings {
    window: iced::window::Settings::default(),
    flags: (init_val, view_func, update_func, eval_cxt),
    default_font: None,
    default_text_size: 20,
    exit_on_close_request: true,
    antialiasing: false,
  };
  MochiApp::run(settings)
}
