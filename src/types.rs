use crate::ast::*;
use lazy_static::lazy_static;
use std::collections::HashMap;

#[derive(Clone, PartialEq, Debug)]
pub enum TypeError {
  Mismatch(Type, Type),
  UnknownVar(String),
  UnknownField(String),
  MissingField(String),
  NonFunctionApp(Type),
  NonArrowLambda(Type),
  NonRecord(Type),
  ProjFromNonRecord(Type),
  SynthLambda,
}

pub type Aliases = HashMap<String, Type>;
pub type Context = HashMap<String, Type>;

lazy_static! {
  pub static ref BASE_ALIASES: HashMap<String, Type> = [
    ("Int".into(), types::prim(PrimType::Int)),
    ("Bool".into(), types::prim(PrimType::Bool)),
    ("Float".into(), types::prim(PrimType::Float)),
    ("String".into(), types::prim(PrimType::String)),
    ("Element".into(), types::prim(PrimType::Element)),
  ]
  .iter()
  .cloned()
  .collect();
}

pub fn drop_aliases(aliases: &Aliases, t: &Type) -> Type {
  match t {
    Type::Named(s) => match aliases.get(s.as_str()) {
      Some(x) => x.clone(),
      None => panic!("unknown type: {}", s),
    },
    Type::Arrow(t1, t2) => {
      let t1 = drop_aliases(aliases, t1);
      let t2 = drop_aliases(aliases, t2);
      types::arrow(
        drop_aliases(aliases, &t1),
        drop_aliases(aliases, &t2),
      )
    }
    Type::Prim(p) => types::prim(*p),
    Type::Record(fields) => Type::Record(
      fields
        .iter()
        .map(|(x, t)| (x.clone(), drop_aliases(aliases, t)))
        .collect(),
    ),
    Type::Variadic(t1, t2) => {
      let t1 = drop_aliases(aliases, t1);
      let t2 = drop_aliases(aliases, t2);
      Type::Variadic(Box::new(t1), Box::new(t2))
    }
  }
}

pub fn drop_term_aliases(aliases: &Aliases, e: &Term) -> Term {
  match e {
    Term::App(e1, e2) => {
      let e1 = drop_term_aliases(aliases, e1);
      let e2 = drop_term_aliases(aliases, e2);
      term::app(e1, e2)
    }
    Term::Ascr(e, t) => {
      let e = drop_term_aliases(aliases, e);
      let t = drop_aliases(aliases, t);
      Term::Ascr(Box::new(e), t)
    }
    Term::If(guard, e1, e2) => {
      let guard = drop_term_aliases(aliases, guard);
      let e1 = drop_term_aliases(aliases, e1);
      let e2 = drop_term_aliases(aliases, e2);
      Term::If(Box::new(guard), Box::new(e1), Box::new(e2))
    }
    Term::Lambda(x, e) => {
      let e = drop_term_aliases(aliases, e);
      Term::Lambda(x.clone(), Box::new(e))
    }
    Term::Literal(l) => Term::Literal(l.clone()),
    Term::Proj(e, label) => {
      let e = drop_term_aliases(aliases, e);
      Term::Proj(Box::new(e), label.clone())
    }
    Term::Record(fields) => Term::Record(
      fields
        .iter()
        .map(|(x, e)| (x.clone(), drop_term_aliases(aliases, e)))
        .collect(),
    ),
    Term::Var(x) => Term::Var(x.clone()),
    Term::Local(defs, body) => Term::Local(
      defs
        .iter()
        .map(|(x, e)| (x.clone(), drop_term_aliases(aliases, e)))
        .collect(),
      Box::new(*body.clone()),
    ),
  }
}

pub fn drop_value_aliases(aliases: &Aliases, v: &Value) -> Value {
  match v {
    Value::Prim(p, args) => Value::Prim(
      *p,
      args
        .iter()
        .map(|v| drop_value_aliases(aliases, v))
        .collect(),
    ),
    Value::Record(fields) => Value::Record(
      fields
        .iter()
        .map(|(x, v)| (x.clone(), drop_value_aliases(aliases, v)))
        .collect(),
    ),
    _ => v.clone(),
  }
}

fn match_types(t1: &Type, t2: &Type) -> bool {
  match (t1, t2) {
    (Type::Variadic(u1, _u2), Type::Arrow(v1, v2))
    | (Type::Arrow(v1, v2), Type::Variadic(u1, _u2)) => {
      match_types(u1, v1) && match_types(t1, v2)
    }
    (Type::Variadic(_, u2), t) | (t, Type::Variadic(_, u2)) => {
      match_types(t, u2)
    }
    _ => t1 == t2,
  }
}

pub fn check(
  cxt: &Context,
  e: &Term,
  t: &Type,
) -> Result<(), TypeError> {
  match e {
    Term::Lambda(x, body) => match t {
      Type::Arrow(dom, cod) => {
        let mut cxt2 = cxt.clone();
        cxt2.insert(x.into(), (**dom).clone());
        check(&cxt2, body, cod)
      }
      _ => Err(TypeError::NonArrowLambda(t.clone())),
    },
    Term::Record(fields) => match t {
      Type::Record(field_types) => {
        // term must at least match all the expected field types, but
        // can have more fields
        field_types.iter().try_for_each(|(x, t)| {
          fields
            .get(x)
            .ok_or_else(|| TypeError::MissingField(x.clone()))
            .and_then(|e1| check(cxt, e1, t))
        })
      }
      _ => Err(TypeError::NonRecord(t.clone())),
    },
    _ => synth(cxt, e).and_then(|t2| {
      if match_types(t, &t2) {
        Ok(())
      } else {
        Err(TypeError::Mismatch(t.clone(), t2))
      }
    }),
  }
}

pub fn synth(cxt: &Context, e: &Term) -> Result<Type, TypeError> {
  match e {
    Term::Var(x) => match cxt.get(x) {
      Some(t) => Ok(t.clone()),
      None => Err(TypeError::UnknownVar(x.clone())),
    },
    Term::App(e1, e2) => synth(cxt, e1).and_then(|t1| match t1 {
      Type::Arrow(dom, cod) => check(cxt, e2, &dom).and(Ok(*cod)),
      Type::Variadic(t1, t2) => {
        check(cxt, e2, &*t1).and(Ok(Type::Variadic(t1, t2)))
      }
      _ => Err(TypeError::NonFunctionApp(t1)),
    }),
    Term::Ascr(e1, t) => check(cxt, e1, t).and(Ok(t.clone())),
    Term::Literal(l) => Ok(l.as_type()),
    Term::Lambda(_, _) => Err(TypeError::SynthLambda),
    Term::Record(fields) => {
      let fields_res: Result<HashMap<String, Type>, TypeError> = fields
        .iter()
        .map(|(x, e1)| synth(cxt, e1).map(|t| (x.clone(), t)))
        .collect();
      fields_res.map(Type::Record)
    }
    Term::Proj(e1, x) => synth(cxt, e1).and_then(|t| match t {
      Type::Record(fields) => fields
        .get(x)
        .cloned()
        .ok_or_else(|| TypeError::UnknownField(x.clone())),
      _ => Err(TypeError::ProjFromNonRecord(t)),
    }),
    Term::If(guard, e1, e2) => {
      check(cxt, guard, &types::prim(PrimType::Bool))?;
      let t1 = synth(cxt, e1)?;
      check(cxt, e2, &t1)?;
      Ok(t1)
    }
    Term::Local(defs, body) => {
      let mut local_cxt = cxt.clone();
      for (x, e) in defs {
        let t = synth(&local_cxt, e)?;
        local_cxt.insert(x.clone(), t);
      }
      synth(&local_cxt, body)
    }
  }
}

#[cfg(test)]
mod tests {

  use super::*;

  const INT_T: Type = Type::Prim(PrimType::Int);
  const BOOL_T: Type = Type::Prim(PrimType::Bool);

  fn var(x: &str) -> Term {
    Term::Var(x.into())
  }

  fn app(t1: Term, t2: Term) -> Term {
    Term::App(Box::new(t1), Box::new(t2))
  }

  fn lam(x: &str, t: Term) -> Term {
    Term::Lambda(x.into(), Box::new(t))
  }

  fn get_cxt() -> Context {
    let mut cxt: Context = HashMap::new();
    cxt.insert("x".into(), INT_T);
    cxt.insert("y".into(), INT_T);
    cxt.insert("b".into(), BOOL_T);
    cxt.insert(
      "plus".into(),
      Type::Arrow(
        Box::new(INT_T),
        Box::new(Type::Arrow(Box::new(INT_T), Box::new(INT_T))),
      ),
    );
    cxt.insert(
      "isEven".into(),
      Type::Arrow(Box::new(INT_T), Box::new(BOOL_T)),
    );
    cxt.insert(
      "apply".into(),
      Type::Arrow(
        Box::new(Type::Arrow(Box::new(INT_T), Box::new(INT_T))),
        Box::new(Type::Arrow(Box::new(INT_T), Box::new(INT_T))),
      ),
    );
    cxt
  }

  fn synth_assert(e: Term, t: Type) {
    let cxt = get_cxt();
    assert_eq!(synth(&cxt, &e), Ok(t));
  }

  fn check_assert(e: Term, t: Type) {
    let cxt = get_cxt();
    assert_eq!(check(&cxt, &e, &t), Ok(()));
  }

  #[test]
  fn var_synth() {
    synth_assert(var("x"), INT_T);
    synth_assert(var("y"), INT_T);
    synth_assert(var("b"), BOOL_T);
    synth_assert(
      var("plus"),
      Type::Arrow(
        Box::new(INT_T),
        Box::new(Type::Arrow(Box::new(INT_T), Box::new(INT_T))),
      ),
    );
    synth_assert(
      var("isEven"),
      Type::Arrow(Box::new(INT_T), Box::new(BOOL_T)),
    );
  }

  #[test]
  fn lambda_check() {
    check_assert(
      lam("z", var("z")),
      Type::Arrow(Box::new(BOOL_T), Box::new(BOOL_T)),
    );
    check_assert(
      lam("z", var("z")),
      Type::Arrow(Box::new(INT_T), Box::new(INT_T)),
    );
    check_assert(
      lam("x", var("x")),
      Type::Arrow(Box::new(BOOL_T), Box::new(BOOL_T)),
    );
  }

  #[test]
  fn app_synth() {
    synth_assert(app(var("isEven"), var("x")), BOOL_T);
    synth_assert(
      app(var("plus"), var("x")),
      Type::Arrow(Box::new(INT_T), Box::new(INT_T)),
    );
    synth_assert(app(app(var("plus"), var("x")), var("y")), INT_T);
    synth_assert(
      app(app(var("apply"), lam("b", var("b"))), var("y")),
      INT_T,
    );
  }

  #[test]
  fn drop_aliases_named() {
    assert_eq!(
      drop_aliases(&BASE_ALIASES, &types::named("Bool")),
      types::prim(PrimType::Bool)
    );
  }

  #[test]
  fn drop_aliases_record() {
    let fields1: HashMap<String, Type> =
      vec![(String::from("pressed"), types::named("Bool"))]
        .iter()
        .cloned()
        .collect();
    let t1 = Type::Record(fields1);
    let fields2: HashMap<String, Type> =
      vec![(String::from("pressed"), types::prim(PrimType::Bool))]
        .iter()
        .cloned()
        .collect();
    let t2 = Type::Record(fields2);
    assert_eq!(drop_aliases(&BASE_ALIASES, &t1), t2);
  }
}
