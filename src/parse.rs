use crate::ast::*;
use nom::branch::alt;
use nom::bytes::complete::is_not;
use nom::bytes::complete::tag;
use nom::character::complete::alphanumeric1;
use nom::character::complete::digit1;
use nom::character::complete::multispace0;
use nom::character::complete::multispace1;
use nom::character::complete::satisfy;
use nom::combinator::eof;
use nom::combinator::map;
use nom::combinator::not;
use nom::combinator::opt;
use nom::combinator::peek;
use nom::error::context;
use nom::multi::many0;
use nom::multi::many1;
use nom::multi::separated_list0;
use nom::multi::separated_list1;
use nom::sequence::delimited;
use nom::sequence::pair;
use nom::sequence::preceded;
use nom::sequence::separated_pair;
use nom::sequence::terminated;
use nom::sequence::tuple;
use nom::IResult;
use nom::Parser;

type Input<'a> = &'a str;
type ParseResult<'a, T> = IResult<Input<'a>, T, ParseError<'a>>;
type ParseError<'a> = nom::error::VerboseError<&'a str>;

fn not_reserved(input: Input) -> ParseResult<()> {
  not(ignore(peek(preceded(
    alt((
      tag("if"),
      tag("then"),
      tag("else"),
      tag("where"),
      tag("let"),
      tag("in"),
    )),
    multispace1,
  ))))(input)
}

fn lower_ident(input: Input) -> ParseResult<&str> {
  preceded(
    preceded(not_reserved, peek(satisfy(|c| c.is_lowercase()))),
    alphanumeric1,
  )(input)
}

fn upper_ident(input: Input) -> ParseResult<&str> {
  preceded(
    preceded(not_reserved, peek(satisfy(|c| c.is_uppercase()))),
    alphanumeric1,
  )(input)
}

fn separator(x: &'static str) -> impl FnMut(Input) -> ParseResult<()> {
  move |input| {
    context(x, ignore(tuple((multispace0, tag(x), multispace0))))(input)
  }
}

fn def_type(input: Input) -> ParseResult<(&str, Type)> {
  context(
    "definition type",
    separated_pair(lower_ident, separator(":"), type_expr),
  )(input)
}

fn def_body(input: Input) -> ParseResult<(&str, Term)> {
  context(
    "definition body",
    map(
      separated_pair(
        pair(lower_ident, many0(preceded(multispace1, pattern))),
        separator("="),
        term_expr,
      ),
      |((x, ys), e)| {
        (x, ys.iter().rev().fold(e, |e, x| term::lambda(x, e)))
      },
    ),
  )(input)
}

fn block(input: Input) -> ParseResult<Vec<Declaration>> {
  preceded(
    preceded(tag("{"), multispace0),
    terminated(
      separated_list0(breaker, preceded(multispace0, definition)),
      context("closing }", preceded(multispace0, tag("}"))),
    ),
  )(input)
}

fn simple_type(input: Input) -> ParseResult<Type> {
  alt((
    // ( TypeExpr )
    delimited(
      preceded(tag("("), multispace0),
      type_expr,
      context("closing paren", preceded(multispace0, tag(")"))),
    ),
    // named types
    map(upper_ident, types::named),
    // record types
    map(
      delimited(
        separator("{"),
        separated_list0(
          separator(","),
          separated_pair(lower_ident, separator(":"), type_expr),
        ),
        context("closing }", separator("}")),
      ),
      |raw_fields| {
        let fields = raw_fields
          .iter()
          .map(|(x, t)| (std::string::String::from(*x), t.clone()))
          .collect();
        Type::Record(fields)
      },
    ),
  ))(input)
}

fn type_expr(input: Input) -> ParseResult<Type> {
  map(
    separated_list1(context("arrow", separator("->")), simple_type),
    |ts: Vec<Type>| {
      ts.iter()
        .rev()
        .cloned()
        .reduce(|t2, t1| types::arrow(t1, t2))
        .unwrap()
    },
  )(input)
}

fn pattern(input: Input) -> ParseResult<&str> {
  // todo: expand this
  lower_ident(input)
}

fn simple_term(input: Input) -> ParseResult<Term> {
  alt((
    // ( TermExpr )
    delimited(
      preceded(tag("("), multispace0),
      term_expr,
      context("closing paren", preceded(multispace0, tag(")"))),
    ),
    // int literal
    map(digit1, |digits: &str| {
      // todo: check to make sure int is in bounds first
      let i = digits.parse::<i64>().expect("Failed to parse int");
      term::int(i)
    }),
    // empty string literal
    // todo: make it so empty is not a special case
    map(tag("\"\""), |_| term::string("")),
    // string literal
    map(
      // todo: allow escaping double quote characters
      delimited(tag("\""), is_not("\""), tag("\"")),
      term::string,
    ),
    // lambda
    map(
      preceded(
        separator("\\"),
        separated_pair(
          separated_list1(pattern, multispace1),
          separator("->"),
          term_expr,
        ),
      ),
      |(xs, e)| xs.iter().rev().fold(e, |e, x| term::lambda(x, e)),
    ),
    // if expressions
    map(
      tuple((
        separator("if"),
        term_expr,
        separator("then"),
        term_expr,
        separator("else"),
        term_expr,
      )),
      |(_, e1, _, e2, _, e3)| {
        let e1 = Box::new(e1);
        let e2 = Box::new(e2);
        let e3 = Box::new(e3);
        Term::If(e1, e2, e3)
      },
    ),
    // record terms
    map(
      delimited(
        separator("{"),
        separated_list0(
          separator(","),
          separated_pair(lower_ident, separator("="), term_expr),
        ),
        preceded(multispace0, tag("}")),
      ),
      |raw_fields| {
        let fields = raw_fields
          .iter()
          .map(|(x, e)| (std::string::String::from(*x), e.clone()))
          .collect();
        Term::Record(fields)
      },
    ),
    // projection
    map(
      separated_pair(lower_ident, tag("."), lower_ident),
      |(e, x)| term::proj(term::var(e), x),
    ),
    // variable
    map(lower_ident, term::var),
    // let
    preceded(
      separator("let"),
      context(
        "let expression",
        map(
          separated_pair(
            context("let bindings", block),
            separator("in"),
            term_expr,
          ),
          |(defs, e)| {
            Term::Local(
              defs
                .iter()
                .map(|d| match d {
                  Declaration::Definition(x, tx, ex) => (
                    x.clone(),
                    Term::Ascr(Box::new(ex.clone()), tx.clone()),
                  ),
                  // todo: return parse error instead of panicking
                  _ => panic!("not a definition"),
                })
                .collect(),
              Box::new(e),
            )
          },
        ),
      ),
    ),
  ))(input)
}

fn let_bindings(input: Input) -> ParseResult<Vec<Declaration>> {
  preceded(
    separator("let"),
    terminated(context("let bindings", block), separator("in")),
  )(input)
}

fn where_bindings(input: Input) -> ParseResult<Vec<Declaration>> {
  preceded(separator("where"), block)(input)
}

fn term_expr(input: Input) -> ParseResult<Term> {
  map(
    tuple((
      opt(let_bindings),
      many1(terminated(simple_term, multispace0)),
    )),
    // safe to unwrap because list must have at least one element
    |(let_defs, es)| {
      let e = es.iter().cloned().reduce(term::app).unwrap();
      let_defs.map_or(e.clone(), |defs| {
        let defs = Declaration::collect_bindings(defs)
          .expect("where bindings must be definitions");
        Term::Local(defs, Box::new(e))
      })
    },
  )(input)
}

fn ignore<'a, T, P>(
  parser: P,
) -> impl FnMut(Input<'a>) -> ParseResult<()>
where
  P: Parser<Input<'a>, T, ParseError<'a>>,
{
  map(parser, |_| ())
}

fn breaker(input: Input) -> ParseResult<()> {
  ignore(preceded(multispace0, tag(";")))(input)
}

fn definition(input: Input) -> ParseResult<Declaration> {
  map(
    tuple((
      def_type,
      preceded(breaker, multispace0),
      def_body,
      opt(where_bindings),
    )),
    |((x1, t), _, (x2, e), opt_defs)| {
      // todo: report a parse error rather than panic
      assert_eq!(x1, x2);
      let e = opt_defs.map_or(e.clone(), |defs| {
        let defs = Declaration::collect_bindings(defs)
          .expect("where bindings must be definitions");
        Term::Local(defs, Box::new(e))
      });
      Declaration::Definition(String::from(x1), t, e)
    },
  )(input)
}

fn declaration(input: Input) -> ParseResult<Declaration> {
  terminated(
    alt((
      // definition
      context("definition", definition),
      // type alias
      context(
        "type alias",
        map(
          preceded(
            separator("type"),
            separated_pair(upper_ident, separator("="), type_expr),
          ),
          |(x, t)| Declaration::TypeAlias(String::from(x), t),
        ),
      ),
      // todo: data definition
    )),
    breaker,
  )(input)
}

fn program(input: Input) -> ParseResult<Program> {
  terminated(
    many0(preceded(multispace0, declaration)),
    preceded(multispace0, eof),
  )(input)
}

pub fn parse_program(
  input: Input,
) -> Result<Program, nom::Err<ParseError>> {
  run_parser(&mut program, input)
}

pub fn run_parser<'a, P, T>(
  parser: &mut P,
  input: Input<'a>,
) -> Result<T, nom::Err<ParseError<'a>>>
where
  P: FnMut(Input) -> ParseResult<T>,
{
  terminated(parser, eof)(input).map(|(_, p)| p)
}

pub fn parse_term(input: Input) -> Result<Term, nom::Err<ParseError>> {
  run_parser(&mut term_expr, input)
}

#[cfg(test)]
mod tests {

  use super::*;
  use crate::ast::term::*;
  use std::collections::HashMap;

  fn parse_declaration(
    input: Input,
  ) -> Result<Declaration, nom::Err<ParseError>> {
    run_parser(&mut declaration, input)
  }

  #[test]
  fn parse_simple_var() {
    assert_eq!(run_parser(&mut simple_term, "x"), Ok(var("x")));
  }

  #[test]
  fn parse_var() {
    assert_eq!(parse_term("x"), Ok(var("x")));
    assert_eq!(parse_term("x1"), Ok(var("x1")));
  }

  #[test]
  fn parse_parens() {
    assert_eq!(parse_term("(x)"), Ok(var("x")));
  }

  #[test]
  fn mismathed_parens_fails() {
    assert!(parse_term("(x").is_err());
    assert!(parse_term("x)").is_err());
  }

  #[test]
  fn parse_var_app() {
    assert_eq!(parse_term("x y"), Ok(app(var("x"), var("y"))));
    assert_eq!(
      parse_term("x y z"),
      Ok(app(app(var("x"), var("y")), var("z")))
    );
    assert_eq!(
      parse_term("x (y z)"),
      Ok(app(var("x"), app(var("y"), var("z"))))
    );
  }

  #[test]
  fn def_value() {
    use Declaration::Definition;
    let actual = run_parser(&mut declaration, "f : Bool; f = true;");
    let expected = Definition(
      String::from("f"),
      Type::Named("Bool".into()),
      var("true"),
    );
    assert_eq!(actual, Ok(expected));
  }

  #[test]
  fn def_func() {
    use types::*;
    use Declaration::Definition;
    let mut actual =
      run_parser(&mut definition, "f : Bool -> Bool; f b = b");
    let mut expected = Definition(
      String::from("f"),
      arrow(named("Bool"), named("Bool")),
      lambda("b", var("b")),
    );
    assert_eq!(actual, Ok(expected));
    actual = run_parser(
      &mut definition,
      "view : Model -> Element; view model = column",
    );
    expected = Definition(
      String::from("view"),
      arrow(named("Model"), named("Element")),
      lambda("model", var("column")),
    );
    assert_eq!(actual, Ok(expected));
  }

  #[test]
  fn prog_test1() {
    use std::fs;
    let raw = fs::read_to_string("tests/programs/test1.mo")
      .expect("read failed");
    let actual = parse_program(raw.as_str());
    assert!(actual.is_ok());
  }

  #[test]
  fn parse_empty_string_literal() {
    assert_eq!(parse_term("\"\""), Ok(string("")));
  }

  #[test]
  fn parse_non_empty_string_literal() {
    assert_eq!(parse_term("\"abc\""), Ok(string("abc")));
  }

  #[test]
  fn column_test() {
    let raw = "(text \"has the button been pressed?\")";
    let actual = parse_term(raw);
    assert_eq!(
      actual,
      Ok(app(var("text"), string("has the button been pressed?")))
    );
    let raw = "column
      (button { label = text \"I am a button!\", onPress = {} })
      (text \"has the button been pressed?\")
      (text (if (model.pressed) then \"yes\" else \"no\"))";
    let actual = parse_term(raw);
    assert!(actual.is_ok());
  }

  #[test]
  fn view_test() {
    let raw = "view : Model -> Element;
    view model =
    column
      (button { label = text \"I am a button!\", onPress = {} })
      (text \"has the button been pressed?\")
      (text (if (model.pressed) then \"yes\" else \"no\"));";
    let actual = parse_declaration(raw);
    assert!(actual.is_ok());
  }

  #[test]
  fn prog_test2() {
    use std::fs;
    let raw = fs::read_to_string("tests/programs/test2.mo")
      .expect("read failed");
    let actual = parse_program(raw.as_str());
    assert!(actual.is_ok());
  }

  #[test]
  fn view_test_prog3() {
    let raw = "column
      (text (app \"counter: \" (intToStr model.counter)))
      (row
        (button { label = text \"-1\", onPress = { change = sub 0 1 } })
        (button { label = text \"+1\", onPress = { change = 1 } }))";
    let actual = parse_term(raw);
    assert!(actual.is_ok());
    let raw = "view : Model -> Element;
view model =
  column
    (text (app \"counter: \" (intToStr model.counter)))
    (row
      (button { label = text \"-1\", onPress = { change = sub 0 1 } })
      (button { label = text \"+1\", onPress = { change = 1 } }));";
    let actual = parse_declaration(raw);
    assert!(actual.is_ok());
  }

  #[test]
  fn prog_test3() {
    use std::fs;
    let raw = fs::read_to_string("tests/programs/test3.mo")
      .expect("read failed");
    let actual = parse_program(raw.as_str());
    assert!(actual.is_ok());
  }

  #[test]
  fn if_test() {
    let (_, actual) = super::term_expr("if x then y else z").unwrap();
    let expected = Term::If(
      Box::new(var("x")),
      Box::new(var("y")),
      Box::new(var("z")),
    );
    assert_eq!(actual, expected);
  }

  #[test]
  fn proj_test() {
    let actual = parse_term("x.y");
    let expected = Term::Proj(Box::new(var("x")), "y".into());
    assert_eq!(actual, Ok(expected));
  }

  #[test]
  fn record_type_alias() {
    let mut actual = parse_declaration("type Model = { };");
    let mut expected = Declaration::TypeAlias(
      "Model".into(),
      Type::Record(HashMap::new()),
    );
    assert_eq!(actual, Ok(expected));
    actual = parse_declaration("type Model = { state : Bool };");
    let mut fields = HashMap::new();
    fields.insert("state".into(), types::named("Bool"));
    expected =
      Declaration::TypeAlias("Model".into(), Type::Record(fields));
    assert_eq!(actual, Ok(expected))
  }

  #[test]
  fn bind_two_vars() {
    use types::*;
    let actual = parse_declaration(
      "update : Message -> Model -> Model;
      update msg model = { counter = add model.counter msg.change };",
    );
    let expected = Declaration::Definition(
      "update".into(),
      arrow(named("Message"), arrow(named("Model"), named("Model"))),
      lambda(
        "msg",
        lambda(
          "model",
          Term::Record(
            [(
              String::from("counter"),
              app(
                app(var("add"), proj(var("model"), "counter")),
                proj(var("msg"), "change"),
              ),
            )]
            .iter()
            .cloned()
            .collect(),
          ),
        ),
      ),
    );
    assert_eq!(actual, Ok(expected));
  }

  #[test]
  fn let_binding_test() {
    let raw =
      "let { x : Int; x = 1; y : Bool; y = true } in if y then x else 0";
    let actual = parse_term(raw);
    let defs = vec![
      (
        String::from("x"),
        Term::Ascr(Box::new(term::int(1)), types::named("Int")),
      ),
      (
        String::from("y"),
        Term::Ascr(Box::new(term::var("true")), types::named("Bool")),
      ),
    ]
    .iter()
    .cloned()
    .collect();
    let expected = Ok(Term::Local(
      defs,
      Box::new(Term::If(
        Box::new(term::var("y")),
        Box::new(term::var("x")),
        Box::new(term::int(0)),
      )),
    ));
    assert_eq!(actual, expected);
  }

  #[test]
  fn where_bindings_test() {
    let raw = "z : Int; z = if y then x else 0 where { x : Int; x = 1; y : Bool; y = true };";
    let actual = parse_declaration(raw);
    let defs = vec![
      (
        String::from("x"),
        Term::Ascr(Box::new(term::int(1)), types::named("Int")),
      ),
      (
        String::from("y"),
        Term::Ascr(Box::new(term::var("true")), types::named("Bool")),
      ),
    ]
    .iter()
    .cloned()
    .collect();
    let expected = Declaration::Definition(
      String::from("z"),
      types::named("Int"),
      Term::Local(
        defs,
        Box::new(Term::If(
          Box::new(term::var("y")),
          Box::new(term::var("x")),
          Box::new(term::int(0)),
        )),
      ),
    );
    assert_eq!(actual, Ok(expected));
  }
}
