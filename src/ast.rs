use iced::Element;
use std::collections::HashMap;

/// Literal values that correspond to basic Rust types.
#[derive(Clone, PartialEq, Debug)]
pub enum Lit {
  Int(i64),
  Float(f64),
  String(String),
  Bool(bool),
}

/// Primitive types consist of the types of literals plus the Element
/// type, which represents GUI elements.
#[derive(Clone, Copy, PartialEq, Debug)]
pub enum PrimType {
  Int,
  Float,
  String,
  Bool,
  Element,
}

/// Primitive operations supported by the language.
#[derive(Clone, Copy, PartialEq, Debug)]
pub enum PrimOp {
  Add,
  Sub,
  Mul,
  StrAppend,
  IntToStr,
}

/// The type forms that Mochi terms can have.
#[derive(Clone, PartialEq, Debug)]
pub enum Type {
  Prim(PrimType),
  Arrow(Box<Type>, Box<Type>),
  Record(HashMap<String, Type>),
  Variadic(Box<Type>, Box<Type>),
  Named(String),
}

/// Convenience functions for creating new Mochi type expressions.
pub mod types {
  use super::Type::*;
  use super::*;

  /// Create a function type from a domain and a codomain type.
  pub fn arrow(t1: Type, t2: Type) -> Type {
    Arrow(Box::new(t1), Box::new(t2))
  }
  /// Create a primitive type expression.
  pub const fn prim(p: PrimType) -> Type {
    Prim(p)
  }
  /// Create a variadic type from the type of its inputs and output.
  pub fn variadic(t1: Type, t2: Type) -> Type {
    Variadic(Box::new(t1), Box::new(t2))
  }
  /// Create a named type from a string.
  pub fn named(x: &str) -> Type {
    Named(x.into())
  }
}

/// Terms in the Mochi AST.
#[derive(Clone, PartialEq, Debug)]
pub enum Term {
  Var(String),
  App(Box<Term>, Box<Term>),
  Lambda(String, Box<Term>),
  Ascr(Box<Term>, Type),
  Literal(Lit),
  Record(HashMap<String, Term>),
  Proj(Box<Term>, String),
  If(Box<Term>, Box<Term>, Box<Term>),
  Local(HashMap<String, Term>, Box<Term>),
}

/// Convenience functions for creating new Mochi terms.
pub mod term {
  use super::Lit;
  use super::Term;
  use super::Term::*;

  /// Create a variable from a string reference.
  pub fn var(s: &str) -> Term {
    Var(String::from(s))
  }
  /// Create an application expression from two terms.
  pub fn app(t1: Term, t2: Term) -> Term {
    App(Box::new(t1), Box::new(t2))
  }
  /// Create a lambda expression from a string and a term.
  pub fn lambda(x: &str, body: Term) -> Term {
    Lambda(String::from(x), Box::new(body))
  }
  /// Create an int literal term.
  pub fn int(i: i64) -> Term {
    Literal(Lit::Int(i))
  }

  /// Create a string literal term.
  pub fn string(s: &str) -> Term {
    Literal(Lit::String(String::from(s)))
  }

  /// Create a projection from a term and a field label.
  pub fn proj(e: Term, label: &str) -> Term {
    Proj(Box::new(e), label.into())
  }
}

/// Values that can be the result of computation on terms.
#[derive(Clone, Debug)]
pub enum Value {
  Int(i64),
  Float(f64),
  String(String),
  Bool(bool),
  Lambda(String, Term),
  Prim(PrimOp, Vec<Value>),
  Element(Box<dyn Elem>, Vec<Value>),
  Record(HashMap<String, Value>),
}

impl PartialEq for Value {
  fn eq(&self, other: &Value) -> bool {
    use Value::*;
    match (self, other) {
      // note: this wasn't able to be derived (not sure why - error was in macro)
      (Element(el1, args1), Element(el2, args2)) => {
        (el1 == el2) && (args1 == args2)
      }
      (Int(v1), Int(v2)) => v1 == v2,
      (Float(v1), Float(v2)) => v1 == v2,
      (String(v1), String(v2)) => v1 == v2,
      (Bool(v1), Bool(v2)) => v1 == v2,
      (Lambda(x1, e1), Lambda(x2, e2)) => (x1 == x2) && (e1 == e2),
      (Prim(op1, args1), Prim(op2, args2)) => {
        (op1 == op2) && (args1 == args2)
      }
      (Record(fields1), Record(fields2)) => fields1 == fields2,
      _ => false,
    }
  }
}

impl Clone for Box<dyn Elem> {
  fn clone(&self) -> Box<dyn Elem> {
    self.clone_as_box()
  }
}

pub trait Elem: Send + Sync + std::fmt::Debug {
  fn render(&self, args: &[Value]) -> Element<'static, Value>;

  fn clone_as_box(&self) -> Box<dyn Elem>;

  fn as_term(&self) -> Term;
}

impl PartialEq<Box<dyn Elem>> for Box<dyn Elem> {
  fn eq(&self, rhs: &Box<dyn Elem>) -> bool {
    self.as_term() == rhs.as_term()
  }
}

/// Declarations modify the context in which terms are evaluated.
#[derive(Clone, PartialEq, Debug)]
pub enum Declaration {
  Definition(String, Type, Term),
  TypeAlias(String, Type),
}

/// A Mochi program consists of a sequence of declarations.
pub type Program = Vec<Declaration>;

impl Lit {
  pub fn as_prim(&self) -> PrimType {
    match self {
      Lit::Int(_) => PrimType::Int,
      Lit::Float(_) => PrimType::Float,
      Lit::String(_) => PrimType::String,
      Lit::Bool(_) => PrimType::Bool,
    }
  }
  pub fn as_type(&self) -> Type {
    Type::Prim(self.as_prim())
  }
  pub fn as_value(&self) -> Value {
    match self {
      Lit::Int(v) => Value::Int(*v),
      Lit::Float(v) => Value::Float(*v),
      Lit::String(v) => Value::String(v.clone()),
      Lit::Bool(v) => Value::Bool(*v),
    }
  }
}

impl PrimOp {
  pub const fn as_str(&self) -> &'static str {
    match self {
      PrimOp::Add => "add",
      PrimOp::Sub => "sub",
      PrimOp::Mul => "mul",
      PrimOp::StrAppend => "app",
      PrimOp::IntToStr => "intToStr",
    }
  }
}

impl PrimType {
  pub const fn as_str(&self) -> &'static str {
    match self {
      PrimType::Int => "Int",
      PrimType::Bool => "Bool",
      PrimType::String => "String",
      PrimType::Float => "Float",
      PrimType::Element => "Element",
    }
  }
  pub fn from_str_opt(s: &str) -> Option<PrimType> {
    match s {
      "Int" => Some(PrimType::Int),
      "Bool" => Some(PrimType::Bool),
      "String" => Some(PrimType::String),
      "Float" => Some(PrimType::Float),
      "Element" => Some(PrimType::Element),
      _ => None,
    }
  }
}

impl Value {
  pub fn as_term(&self) -> Term {
    match self {
      Value::Int(v) => Term::Literal(Lit::Int(*v)),
      Value::Float(v) => Term::Literal(Lit::Float(*v)),
      Value::String(s) => Term::Literal(Lit::String(s.clone())),
      Value::Bool(v) => Term::Literal(Lit::Bool(*v)),
      Value::Lambda(x, body) => {
        Term::Lambda(x.clone(), Box::new(body.clone()))
      }
      Value::Prim(op, args) => {
        let mut e = Term::Var(op.as_str().into());
        for arg in args {
          e = Term::App(Box::new(e), Box::new(arg.as_term()));
        }
        e
      }
      Value::Record(fields) => {
        let field_terms = fields
          .iter()
          .map(|(x, v)| (x.clone(), v.as_term()))
          .collect();
        Term::Record(field_terms)
      }
      Value::Element(el, args) => {
        let mut e = el.as_term();
        for arg in args {
          e = Term::App(Box::new(e), Box::new(arg.as_term()));
        }
        e
      }
    }
  }
  pub fn render(&self) -> Element<'static, Value> {
    match self {
      Value::Element(el, args) => el.render(args),
      _ => panic!("value is not an element"),
    }
  }
}

impl Declaration {
  pub fn collect_bindings(
    ds: Vec<Declaration>,
  ) -> Option<HashMap<String, Term>> {
    let mut defs = HashMap::new();
    // todo: better way to do this?
    let mut all_good = true;
    ds.iter().for_each(|d| match d {
      Declaration::Definition(x, t, e) => {
        defs.insert(
          x.clone(),
          Term::Ascr(Box::new(e.clone()), t.clone()),
        );
      }
      _ => {
        all_good = false;
      }
    });
    if all_good {
      Some(defs)
    } else {
      None
    }
  }
}
