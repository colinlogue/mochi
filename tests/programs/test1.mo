
type Model = {};
type Message = {};


init : Model;
init = {};

view : Model -> Element;
view model = column;

update : Message -> Model -> Model;
update msg model = model;

