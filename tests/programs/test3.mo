type Model = { counter : Int };
type Message = { change : Int };

init : Model;
init = { counter = 0 };

view : Model -> Element;
view model =
  column
    (text (app "counter: " (intToStr model.counter)))
    (row
      (button { label = text "-1", onPress = { change = sub 0 1 } })
      (button { label = text "+1", onPress = { change = 1 } }));

update : Message -> Model -> Model;
update msg model = { counter = add model.counter msg.change };