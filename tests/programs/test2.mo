type Model = { pressed : Bool };
type Message = {};


init : Model;
init = { pressed = false };

view : Model -> Element;
view model =
  column
    (button { label = text "I am a button!", onPress = {} })
    (text "has the button been pressed?")
    (text (if (model.pressed) then "yes" else "no"));

update : Message -> Model -> Model;
update msg model = { pressed = true };