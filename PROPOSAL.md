# Project Proposal

Colin Logue, clogue@pdx.edu

https://gitlab.com/colinlogue/mochi

For my Rust course project, I plan to develop an interpreter
for a pure functional language for GUI applications, similar
to Elm but not limited to web browsers. I will be using the
Iced library for the GUI elements, which is based on the Elm
architecture, and the pest library for parsing (or at least
for lexing).

## The Mochi language
The language is named Mochi, after my brother's cat who
fervently believes that the correct spot for a cat to be is
always between a human and the computer monitor that person
is trying to see.

To keep things simple, I will be starting out with a pretty
restricted language, essentially the STLC extended with
records and several primitives types. These primitives will
include the basic integer, boolean, and string types, but
also a type Element which will represent bindings to the
underlying Iced API.

A program will be a series of top-level declarations
defining functions, constants, and custom types (which for
now will just be records, but I may try to extend to
algebraic data types depending on how things go). A valid
program defines types `Model` and `Message`, as well as
functions `view` (type `Model -> Element`) and `update`
(type `Message -> Model -> Model`). The `view` function
takes a `Model` (which represents the state of the program)
and returns an `Element` (how the program should render with
that state) and the `update` function takes a `Message`
(which is fired on defined user interactions such as
clicking a button or entering text) and a `Model` (the state
before the interaction) and returns the updated state after
the event.

Finally, a constant called `init` of type `Model` defines
the initial state of the program.

Here is an example program that would produce a row with
three elements: a button on the left labelled "-" that sends
a message to decrement a counter; a button on the right that
sends a message to increment a counter; and a text node in
between the two that displays the counter:

```elm
type Model = { counter : Int }

type Message = { op : String }

init : Model
init = { counter = 0 }

view : Model -> Element
view model =
  -- row is a magic variadic primitive that takes an
  -- arbitrary number of Elements and produces an Element
  row
    -- button has type Element -> Message -> Element
    (button (text "-") { op = "decr" })

    (text (intToString model.counter))

    (button (text "+") { op = "incr" })

update : Message -> Model -> Model
update msg model =
  if msg.op == "incr"
    then { counter = model.counter + 1 }
  else if msg.op == "decr"
    then { counter = model.counter - 1 }
  else
    model
```

As I'm not planning to have a list type in the initial
version, the `row` and `column` primitives that correspond
to `iced::widget::Row` and `iced::widget::Column` will be
variadic to allow them to contain arbitrary numbers of
elements.

## Running Mochi programs
The Mochi interpreter will be a Rust program that reads in a
file path from stdin, parses the file into a Mochi AST, then
builds an `iced::Sandbox` from the AST and runs it.

```mermaid
graph TB
  1([Mochi source file]) -- pest lexer -->
  2([Lexical tokens]) -- shift-reduce parser -->
  3([AST]) -- typechecker --> 3
  3 -- interpreter -->
  4([Rust representation]) -- run Iced application -->
  5([GUI app])
```

### Lexing phase
I will be using the pest library for parsing the source file
into a sequence of lexical tokens. Pest reads in a grammar
from a .pest file and derives a Rust parser for you (which
is awesome). I toyed around with doing the entire parser
with pest, but there are some limitations of the grammar
format that I don't want to work around. But for producing a
flat list of tokens, it seems perfect.

### Parsing phase
From the token list, I will write a shift-reduce parser to
produce a Mochi AST. The basic idea is that you have an
initially-empty stack of symbols that represent the grammar
rules (of which the tokens are the terminals). At each step,
push (shift) the first token from the list to the stack and
check if the symbols on the stack match any grammar rules,
and if they do, reduce the stack by replacing the matched
symbols with the corresponding rule. Continue reducing until
no rules match, and then shift again and repeat until the
input list is empty. A successful parse ends with a single
symbol on the stack the holds the parsed AST.

I believe that Rust's match expressions for vector slices
should work well for this, although I haven't tried it out
yet so I expect there will be all sorts of fun figuring out
the details of how everything interacts with borrowing.

### Typechecking phase
After building the AST, it will get typechecked before being
passed to the interpreter. I'll probably also have a flag to
run just the typechecker on its own. While this phase will
eventually be the most interesting, for now I've designed
the type system so that it is fairly trivial to check types.
With no type variables and all functions requiring explicit
ascriptions, every top-level term should have an inferrable
concrete type.

### Interpreter phase
The interpreter will (most likely) be a Rust struct that
implements the `iced::Sandbox` trait, with fields that store
the view and update functions and the model state. Executing
a program will involve generating appropriate view and
update functions and storing them (or references to them) in
the struct. The Message type will likely be the AST type,
and the model will be a HashMap with String keys and AST
values. The view and update methods of the `iced::Sandbox`
implementations will then just call the respective generated
function.

A near-future stop on the Mochi roadmap will be to replace
the interpreter with a code generator.

### Runtime
Once the Iced application is created, it should be a simple
matter to invoke the run function from `main`, which will
then run the generated GUI program.