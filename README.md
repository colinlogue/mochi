# The Mochi Programming Language

Mochi is a pure functional language for creating GUI apps.
It is strongly based on Elm, but is not limited to web. It
is implemented in Rust and uses the `iced` library for
rendering.

The project documentation is
[here](https://colinlogue.gitlab.io/mochi/doc/mochi/).

## Overview
Mochi uses The Elm Architecture, a pattern that abstracts an
interactive program into four concepts:

1.  A `Model` type that represents the state of the program
2.  A `Message` type that represents events (such as user
    input) that affect the state
3.  A `view` function that describes how the app should look
    for each possible state
4.  An `update` function that describes how the state should
    change when it receives a message

A Mochi program must define all of these, as well as an
initial `Model` value that the program is initialized with.
A runtime system then interprets the program, handling the
rendering and user interaction based on the declarative
specification.

## Status
Mochi is currently at a very early proof-of-concept stage of
development. There is a small handful of primitive types but
no polymorphism or user-defined types yet. GUI elements are
limited to rows, columns, text nodes and buttons. But the
interpreter can read files, typecheck them and run them if
they are valid.

## Running the interpreter
To run a Mochi program, clone this crate with `git clone
https://gitlab.com/colinlogue/mochi` and then use `cargo run
--bin=interp <filepath>`, where `<filepath>` is the name of the
mochi file to run. It may take a while to build the first
time, as there are quite a few inherited dependencies.

## Syntax
The language is very similar to Elm, although currently
missing features such as polymorphic types and infix
operators. A simple but complete Mochi program looks like
this:

```
type Model = { pressed : Bool; };
type Message = {};


init : Model
init = { pressed = false; };

view : Model -> Element
view model =
  column
    (button { label = text "I am a button!"; onPress = {}; })
    (text "has the button been pressed?")
    (text (if (model.pressed) then "yes" else "no"));

update : Message -> Model -> Model
update msg model = { pressed = true; };
```
This produces a GUI app that looks like this:

![Demonstration a Mochi program](anim.gif)

The semicolons are temporary features to make parsing
simpler and will not be required in future versions.
Unfortunately there are not yet informative error messages

## Type system
At the moment, types are limited to a small set of
primitive types (Int, Float, Bool, String and Element) as
well as functions and record types. There are additionally
two variadic primitives, `column` and `row`, which act as
functions that can take an arbitrary number of Element
inputs and produce an Element value. Once lists are
implemented, the variadic types will be removed and they
will instead take lists of Element as input.

## Roadmap
In no particular order, here are some near-term goals of the
language:
- User-defined algebraic data types (and match expressions
to go with them)
- Type polymorphism
- Friendly, helpful error messages (this is one of Elm's
  best features, and is a high priority)
- Compiled binaries
- A prelude and standard library
- Interaction with Rust code (haven't thought through how
  this would work but it sounds cool)

## Namesake
Mochi is the name of my brother's cat, who very much wanted
to be involved as I was starting to work on this. And by
"be involved" I mean "sit between me and my monitor."